from django.contrib.auth.models import User
from rest_framework import serializers
from todos.models import Todo


class TodoSerializer(serializers.ModelSerializer):

    class Meta:
        model = Todo
        fields = ('id', 'title', 'description', 'completed')


class TodoUserSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = ("id", "email", "date_joined")

